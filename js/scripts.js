function SocialSharerElement() {
  this.$tag = undefined;
  this.params = undefined;
  this.type = undefined;
}

SocialSharerElement.prototype.init = function (tag) {
  this.$tag = jQuery(tag);

  this.params = JSON.parse(this.$tag.attr('data-params'));
  this.type = this.$tag.attr('data-type');

  this.$tag.click(jQuery.proxy(this.onClick, this));
};

//private
SocialSharerElement.prototype.onClick = function (event) {
  event.preventDefault();
  if (this.type == 'facebook') {
    SocialSharer.shareFacebook(this.params.url);
  } else if (this.type == 'twitter') {
    SocialSharer.shareTwitter(this.params.url, this.params.text);
  } else if (this.type == 'pinterest') {
    SocialSharer.sharePinterest(this.params.url);
  }
};

(function (jQuery) {
  Drupal.behaviors.social_sharer_links = {
    attach: function (context, settings) {
      jQuery('.social-sharer-links a', context).once('social_sharer_links').each(function (index, element) {
        var socialSharerElement = new SocialSharerElement();
        socialSharerElement.init(element);
       });
     }
  };
})(jQuery);
