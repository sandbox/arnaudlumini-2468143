<div class="social-sharer-links">
  <span><?= t('Share') ?></span>
  <a href="" class="facebook" data-params='<?= $params ?>' data-type="facebook">facebook</a>
  <a href="" class="twitter" data-params='<?= $params ?>' data-type="twitter">twitter</a>
  <a href="" class="pinterest" data-params='<?= $params ?>' data-type="pinterest">pinterest</a>
</div>
